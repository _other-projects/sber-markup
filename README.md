Запуск проекта локально:

1) npm i
2) npm start

Создание билда проекта:
 - npm build

СТРАНИЦЫ ПРОЕКТА:

* https://_other-projects.gitlab.io/sber-markup/index.html
* https://_other-projects.gitlab.io/sber-markup/analitic.html
* https://_other-projects.gitlab.io/sber-markup/analitic-apartment.html
* https://_other-projects.gitlab.io/sber-markup/analitic-auto.html
* https://_other-projects.gitlab.io/sber-markup/estimate-apartment.html
* https://_other-projects.gitlab.io/sber-markup/estimate-auto.html
* https://_other-projects.gitlab.io/sber-markup/estimate-room.html
* https://_other-projects.gitlab.io/sber-markup/page-with-charts.html
